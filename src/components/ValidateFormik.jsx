import React, { Component } from "react";
import { Formik } from 'formik';
import * as Yup from "yup";

export default class ValidateFormik extends Component {
    constructor(){
        super(); 
        this.state = {
            obj : [
            ]
        }
    }
  render() {
    return (
      <div>
        <div class="text-5xl text-center pt-10 font-bold ">
          <h1 class="bg-gradient-to-r from-blue-600 via-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">
            FORM LOGIN
          </h1>
        </div>
        <Formik
        initialValues={{ username: "", password: "" }}
        validationSchema=
        {Yup.object({
          username: Yup.string()
            .max(15, "Must be 15 characters or less")
            .min(5, "Must be 5 characters or more")
            .matches(/^[a-z|A-Z]+$/, "You must input string only")
            .required("Input your name !"),
          password: Yup.string()
            .min(8, "Must be 8 characters with digit and special symbol")
            .max(16,"Must be less than 16")
            .matches(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/,"Must contain characters, digit and special symbol")
            .required("Input your password !"),
        })}
        onSubmit=
        {(values, { setSubmitting }) => {
          setTimeout(() => {
            this.setState({ obj: [...this.state.obj, values] });
            setSubmitting(false);
          }, 400);
        }}
        >
        {({
          values,
          handleChange,
          errors,
          handleBlur,
          touched,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            <div class=" w-[70%] m-auto mt-4 ">
              <label
                for="input-group-1"
                class="block mb-2 font-medium text-gray-900 dark:text-black text-xl"
              >
                Username
              </label>
              <div class="relative mb-6">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none text-gray-600 font-bold">
                  😃
                </div>
                <input
                  type="text"
                  name="username"
                  value={values.username}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  class=" text-gray-900 text-sm rounded-md  block w-full pl-10 p-4  dark:bg-white dark:border-gray-600 dark:placeholder-black dark:text-black font-semibold"
                  placeholder="Enter your name"

                  
                />
              </div>

              <div className="text-center">{errors.username && touched.username && errors.username} </div>
              

              <label
                for="input-group-1"
                class="block mb-2 font-medium text-gray-900 dark:text-black text-xl"
              >
                Password
              </label>
              <div className="relative mb-6">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  🔒
                </div>
                <input
                  type="password"
                  name="password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className=" text-gray-900 text-sm rounded-md  block w-full pl-10 p-4  dark:bg-white dark:border-gray-600 dark:placeholder-black dark:text-black font-semibold"
                  placeholder="Enter your password"
                />
              </div>
              <div class="text-center">{errors.password && touched.password && errors.password} </div>
              

              <div className="text-center">
                <button
                  type="submit"
                  class="  p-3 mt-6 w-[250px] rounded-md font-bold text-center text-white bg-blue-500 border-2 border-blue-500 "
                  disabled={isSubmitting}
                >
                  Submit
                </button>
              </div>
            </div>
          </form>
        )}
        </Formik>

        <div class="relative overflow-x-auto mt-12 text-center ">
    <table class="w-[70%] text-xl text-left text-black dark:text-black m-auto p-4">
        <thead class="text-md text-black dark:bg-slate-500 dark:text-white bg-gradient-to-r from-blue-600 via-green-500 to-indigo-400">
            <tr>
                <th scope="col" class="p-5 text-center rounded-l-md ">
                    Username
                </th>
                <th scope="col" class="p-5 text-center rounded-r-md">
                    Password
                </th>
            </tr>
        </thead>
        <tbody>
                {this.state.obj.map((items)=>(
                    <tr  class="p-10 h-16 text-md text-center font-semibold text-black bg-slate-200" key={items}  >
                        <td class="rounded-l-md">{items.username}</td>
                        <td className="rounded-r-md">{items.password}</td>
                        
                    </tr>
                   
                ))}
        </tbody>
    </table>

      </div>
      </div>
    );
  }
}
