
import './App.css';
import ValidateFormik from './components/ValidateFormik';

function App() {
  return (
    <div>
        <ValidateFormik/>
    </div>
  );
}

export default App;
